# -*- coding: utf-8 -*-
import cherrypy
from vispa.server import AbstractExtension
from controller import ExamplesController

from vispa.controller import AbstractController


class ExamplesExtension(AbstractExtension):

    def name(self):
        return "examples"

    def dependencies(self):
        return []

    def setup(self):
        self.add_controller(ExamplesController(self))
        self.add_workspace_directoy()
