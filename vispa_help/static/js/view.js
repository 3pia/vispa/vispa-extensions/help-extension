define(["jquery"], function($) {

  var renderDocuExamplesView = function(view, node, request) {
    var self = this;
    view.node = node;

    view.setLoading(true);
    // get path to html from workspace.ini
    view.GET("/ajax/fs/getworkspaceini", {
      "request": JSON.stringify({
        "PathToTemplates": request
      })
    }, function(err, res) {
      if (err) return;

      // get content from html and load it
      view.GET("/ajax/fs/getfile", {
        "path": res.content.PathToTemplates[request]
      }, function(err, res2) {
        if (err) return;

        node.append(res2.content);
        view.applyPreferences();
        if (view.viewSetupCallback !== null) {
          view.viewSetupCallback.call(view);
        }
        view.setLoading(false);
      });
    });
  };

  return renderDocuExamplesView;
});