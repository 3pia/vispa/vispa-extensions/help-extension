define([
  "vispa/extension",
  "vispa/views/main",
  "jquery",
  "./view"
], function (Extension,
             MainView,
             $,
             renderDocuExamplesView) {

  var ExampleExtension = Extension._extend({
    init: function init() {
      var self = this;
      init._super.call(self, "examples","Examples");
      //add views and menu entries
      self.mainMenuAdd([
        self.addView(ExampleView),
        self.addView(DocumentationView)
      ]);
      /*
      self.setOptions(ExampleView, {
        "maxInstances": 1
      });
      self.setOptions(DocumentationView, {
        "maxInstances": 1
      });
      //show the button
      self.getDefaultPreferences(ExampleView).fastMenuEntries.value = [ //??? Why not shown in tab? ??
        "copyall"
      ];
      //preferences
      self.setDefaultPreferences("example", Preferences.getDefaultExamplePreferences(), {
        title: "Examples"
      });
      //preferences
      self.setDefaultPreferences("documentation", Preferences.getDefaultDocumentationPreferences(), {
        title: "Installed Software"
      });
      //add global callbacks
      vispa.callbacks.on("showExamples", function (workspaceId) {
        self.createInstance(workspaceId, ExampleView);
      });
      vispa.callbacks.on("showDocs", function (workspaceId) {
        self.createInstance(workspaceId, DocumentationView);
      });
      */
    }
  });

  var ExampleView = MainView._extend({
    init: function init(args) {
      var self = this;
      init._super.apply(self, arguments);
      //init preference variables
      self.examplePath = "";
      self.examplePaths = [];
      self.execute = false;
      self.viewSetupCallback = self.setupButton;
      self.node = null;
      self.workspaceini = null;
      //copy all button //TODO!: Button should show up in the tab bar
    },

    render: function (node) {
      renderDocuExamplesView(this, node, "ExampleHTMLPath");
    },

    // function to apply preferences
    applyPreferences: function () {
      this.examplePath = this.prefs.get('CopyPath');
    },

    //what happens before the instance is maximized
    onBeforeShow: function () {
      var self = this;
      $.each($(".btn-reset"), function (i, elem) {
        var filename = self.getfilename($(elem).parent().data("copy"));
        self.toggleResetButtonCSS($(elem), filename);
      });
    },

    //empty function in which the events are assigned to the buttons
    setupButton: function () {
      var self = this;
      self.createDestination();
      $.each($(self.node).find('.button-section'), function (i, elem) {
        var section = $(elem);
        var file = section.data("file");
        var copyPath = section.data("copy");
        self.examplePaths.push(copyPath);
        var extension = section.data("extension");
        $.each(section.find(".btn-primary"), function (i, elem2) {
          var obj = $(elem2);
          if (obj.hasClass("btn-reset")) {
            self.toggleResetButtonCSS(obj, file);
            $(obj).click(function () {
              self.confirm("Do you want to overwrite this example?",
                  function (confirmed) {
                    if (confirmed) {
                      self.overwrite(copyPath, self.examplePath);
                    }
                  });
            });
          }
          if(obj.hasClass("btn-open")) {
            obj.click(function () {
              self.buttonClicked(section.data());
            });
          }
        });
      });
    },

    toggleResetButtonCSS: function (btn, filename) {
      var self = this;
      self.GET("/ajax/fs/exists", {
        path: self.abspath(self.examplePath, filename)
      }, function (err, rtn) {
        if (rtn == "Failed") {
          btn.css("visibility", "hidden");
        } else {
          btn.css("visibility", "visible");
        }
      });
    },

    // Function to setup the action that should happen, if a button is clicked.
    // Within this function, the different types of extensions should be distinguished
    buttonClicked: function (data) {
      var self = this;
      if (data.extension == 'CodeEditor') {
        self.GET("/ajax/fs/exists", {
          path: self.abspath(self.examplePath, data.file)
        }, function (err, res) {
          if (res == "Failed") {
            self.copy(data.copy, self.examplePath, self.openEditor, data.file);
          } else {
            self.openEditor(self.abspath(self.examplePath, data.file));
          }
        });
      } 
      else if (data.extension == "PXLDesigner") {
        self.GET("/ajax/fs/exists", {
          path: self.abspath(self.examplePath, data.file)
        }, function (err, res) {
          if (res == "Failed") {
            self.copy(data.copy, self.examplePath, self.openPXLDesigner, data.file);
          } else {
            self.openPXLDesigner(self.abspath(self.examplePath, data.file));
          }
        });
      }
      else if (data.extension == "FileBrowser") {
        self.GET("/ajax/fs/exists", {
          path: self.abspath(self.examplePath, data.file)
        }, function (err, res) {
          if (res == "Failed") {
            self.copy(data.copy, self.examplePath, self.openFileBrowser, data.file);
          } else {
            self.openFileBrowser(self.abspath(self.examplePath, data.file));
          }
        });
      }
      else if(data.extension=="augerofflineeventbrowser"){
        self.openAugerEventBrowser(data.file.filenames,data.tabname,data.tab.content);
      }
      else { //TODO: to be extended with more if statements in further versions
        self.alert("No extension to open this file!"); //TODO! (text)
      }
    },

    //empty function to open the selected file in code editor (more specialized version func in comparison to 'open_file')
    openEditor: function (path) {
      this.spawnInstance("codeeditor", "CodeEditor", {path: path});
    },
    
    openPXLDesigner: function (path) {
      this.spawnInstance("pxldesigner", "PXLDesignerView", {path: path});
    },

    openAugerEventBrowser: function(path,tabName,tabContent){
      this.spawnInstance("augerofflineeventbrowser","AugerOfflineEventbrowserView",{path:path,tabName:tabName,tabContent:tabContent});
    },
    
    openFileBrowser: function(path) {
    	this.spawnInstance("file2", "FileMain", {path: path});
    },
    
    createDestination: function () {
      var self = this;
      if (self.examplePath !== "") {
        self.POST("check_destination", {
          path: self.examplePath
        }, function (err, res) {
          if (res.success === false) {
            self.alert("There was an error: " + res.type +
                " The example extension has been closed.");
            self.close();
          } else {
            if (res.type == "f") {
              self.alert(
                  /*jshint multistr: true */
                  "Examples destination is a file and not a directory. \
                Please change the path for the examples in the preferences. \
                The example extension has been closed."
              );
              self.close();
              return;
            }
            if (!res.exist) {
              self.POST("create_folder", {
                path: self.examplePath
              }, function(err, res2) {
                if(err == null) {
                  self.alert("Could not create destination path. Reason: " + res2.exception +
                      " The example extension has been closed.");
                  self.close();
                }
              });
            }
          }
        })
      }
    },

    copy: function (src, dest, callback, cbargs) {
      var self = this;
      callback = callback === undefined ? null : callback;
      //var filename = self.getfilename(src);
      self.POST("/ajax/fs/paste", {
        "path": dest,
        "paths": JSON.stringify(src),
        "cut": false
      }, function (err) {
        if (err == null) {
          if (callback !== null) {
            callback.call(self, self.abspath(dest, cbargs));
          }
        }
      });
    },

    overwrite: function (src, dest) {
      var self = this;
      var filename = self.getfilename(src);
      self.POST("/ajax/fs/remove", {
        "path": JSON.stringify(self.abspath(dest, filename))
      }, function (err) {
        if (err == null) {
          self.copy(src, dest);
        }
      });
    },

    //empty function to copy each example in the indicated path
    copyAll: function () {
      var self = this;
      var dest = self.examplePath;
      self.confirm("Do you want to overwrite all examples in " + self.examplePath + "?",
          function (confirmed) {
            if (confirmed) {
              $.each(self.examplePaths, function (index, elem) {
                self.GET("/ajax/fs/exists", {
                  path: self.abspath(self.examplePath, self.getfilename(elem))
                }, function (err, res) {
                  if (err == null) {
                    if (res == "Failed") {
                      self.copy(elem, dest, null);
                    } else {
                      self.overwrite(elem, dest);
                    }
                  }
                });
              });
            }
          });
    },

    getfilename: function (path) {
      return path.split('/').pop();
    },

    abspath: function (path, name) {
      var len = this.examplePath.length;
      if (name.substr(0, len) == this.examplePath) {
        return name;
      }
      return (path + "/" + name).replace(/\/{2,}/g, "/");
    }

  },{
    iconClass: "glyphicon glyphicon-play-circle",
    label: "Examples",
    name: "Examples",
    menuPosition: 70,
    maxInstances: 1,
    menu: {
      copyAll:{
        label: "copy all",
        iconClass: "glyphicon glyphicon-share-alt",
        callback: function(){
          this.$root.instance.copyAll();
        }
      }
    },
    preferences: {
      items: {
        CopyPath: {
          label: "CopyPath",
          level: "1",
          type: "string",
          value: "$HOME",
          description: "Folder into which the example files are copied"
        }
      }
    }
  });

  var DocumentationView = MainView._extend({
    init: function init(args) {
      var self = this;
      init._super.apply(self, arguments);
      //init preference variables
      self.execute = false;
      self.viewSetupCallback = null;
      self.node = null;
    },

    render: function (node) {
      renderDocuExamplesView(this, node, "DocumentationHTMLPath");
    },

    //empty function to apply preferences
    applyPreferences: function () {
    }

  },{
    iconClass: "glyphicon glyphicon-book",
    label: "Documentation",
    name: "Documentation",
    menuPosition: 71
  });

  return ExampleExtension;
});