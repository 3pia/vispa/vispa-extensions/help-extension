# -*- coding: utf-8 -*-

# imports
import cherrypy
import vispa.workspace
from vispa.controller import AbstractController


class ExamplesController(AbstractController):

    def __init__(self, extension):
        AbstractController.__init__(self)
        self.extension = extension

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    def create_folder(self, path):
        self.release_session()
        rpc = self.get("proxy", "ExamplesRpc")
        self.release_database()
        return rpc.create_folder(path)

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    def check_destination(self, path):
        self.release_session()
        rpc = self.get("proxy", "ExamplesRpc")
        self.release_database()
        return rpc.check_destination(path)
    '''
    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    def rsync(self, src, dest, opt):
        self.release_session()
        rpc = self.get("proxy", "ExamplesRpc")
        self.release_database()
        return rpc.rsync(src, dest, opt)
    '''


