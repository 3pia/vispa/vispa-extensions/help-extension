# -*- coding: utf-8 -*-

import os
import logging
import json
import ConfigParser
from threading import Thread
import shutil
import subprocess

logger = logging.getLogger(__name__)


def expand(path):
    return os.path.expanduser(os.path.expandvars(path))


class ExamplesRpc:

    def __init__(self):
        logger.debug("ExamplesRpc created")
        self._popen = None

    def create_folder(self, path):
        path = os.path.expanduser(os.path.expandvars(path))
        try:
            os.makedirs(path)
            response = {
                "success": True
            }
            return json.dumps(response)
        except Exception as e:
            # raise Exception("You don't have the permission to create this folder!")
            # raise Exception(str(e))
            response = {
                "success": False,
                "exception": str(e)
            }
            return json.dumps(response)

    def check_destination(self, path):
        path = os.path.expanduser(os.path.expandvars(path))
        try:
            if not os.path.exists(path):
                return json.dumps({"success": True, "exist": False, 'type': None})
            else:
                target_type = 'd' if os.path.isdir(path) else 'f'
                return json.dumps({"success": True, "exist": True, "type": target_type})
        except Exception as e:
            return json.dumps({"success": False, "exist": None, "type": str(e)})
    '''
    def rsync(self, src, dest, opt):
        cmd = "rsync"
        args = [cmd, opt, expand(src), expand(dest)]
        self._popen = subprocess.Popen(args)
        return json.dumps({"success": True})
    '''